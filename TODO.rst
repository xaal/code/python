
List of things for to be done
=============================

Gateways
--------
* Rewrite the NetAtmo (they trashed the old API)

* Switch to async API for Yeelight (this should be a good example)

* Fix button sending wrong notification (aqara, edisio)

Apps
----
* Lint / Check in rest/fakeinput/dashboard


Lib
---
* Add a default command line parser:
  The idea is to provide a way to override default config
