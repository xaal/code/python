import asyncio
from re import I
import socketio
import time
from xaal.lib import Device,tools
from xaal.lib import AsyncEngine as Engine
import logging


logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


logging.getLogger('socketio.client').setLevel(logging.WARNING)
logging.getLogger('engineio').setLevel(logging.WARNING)

sio = socketio.AsyncClient(engineio_logger=True,ssl_verify=False)
dev = Device('stupid.basic',tools.get_random_uuid())




@sio.event
async def connect():
    print('connection established')


@sio.event
def installation(data, useBI):
    #import pdb;pdb.set_trace()

    alarms = data['alarms']
    for k in alarms:
        print(k)
        print()

@sio.event
async def event_attributeChanges(data):
    print('attributeChange evt:', data)
    addr = tools.get_uuid(data['address'])
    if addr != dev.address: # it's not my own message ? 
        dev.attributes[0].value = int(time.time())



def installation_new(data):
    alarms = data['alarms']
    for k in alarms:
        print(k)
        print()


def activity_on_fields(data):
    #print(data)
    pass

def fall_detected(*args):
    #print("FALL")
    print(args)

def alarms_detected(ff):
    print(ff)
    #import pdb;pdb.set_trace()



sio.on('installation-new',installation_new)
sio.on('new-activity-on-fields',activity_on_fields)
#sio.on('falls-detected',fall_detected)
sio.on('alarms-detected',alarms_detected)



@sio.event
async def disconnect():
    print('disconnected from server')





async def run_sio():
    await sio.connect('http://floor1.enstb.org:8000/')#transports='polling')
    #await sio.connect('http://localhost:9099/',transports='websocket')
    await sio.wait()



async def run_xaal():
    eng = Engine()
    dev.new_attribute('last_msg')
    eng.add_device(dev)
    await eng.run()

if __name__ == '__main__':
    tasks = [   asyncio.ensure_future(run_sio()), asyncio.ensure_future(run_xaal()), ]
    #tasks = [   asyncio.ensure_future(run_sio()), ]
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(asyncio.wait(tasks))
    except KeyboardInterrupt:
        print('Bye')
