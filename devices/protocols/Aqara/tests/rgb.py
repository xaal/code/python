

def rgb_test(red,green,blue,brightness=0xFF):
    rgb = brightness<<24|( red << 16)|( green << 8)|blue
    print("rgb: %s" % rgb)

    brightness = (rgb >> 24) & 0xFF
    r = ((rgb >> 16) & 0xFF)
    g = ((rgb >> 8) & 0xFF)
    b = (rgb & 0xFF)

    print(f"rgb: {brightness} {r} {g} {b}")


rgb_test(19,199,30,100)
