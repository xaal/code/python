import asyncio
from xaal.lib import AsyncEngine,tools
from xaal.schemas import devices
import sys

LAMPS=['cdef8707-f268-11e9-84a2-7085c2a4a6bd','41998f5e-b1a6-11ec-b598-d6bd5fe18701']


def run():
    eng = AsyncEngine()
    dev = devices.hmi(tools.get_random_uuid())
    eng.add_device(dev)
    eng.start()
   
    async def loop():
        lamps = [tools.get_uuid(l) for l in LAMPS]
        color = 0
        if len(sys.argv) > 0:
            color = int(sys.argv[1])
        while 1:
            eng.send_request(dev,lamps,'set_hsv',{'hsv':(color,1,1),'smooth':400 })
            color = color + 3
            if color > 360:color = 0
            await asyncio.sleep(2)
    
    eng.on_start(loop)
    eng.run()
try:
    run()
except KeyboardInterrupt:
    print('Bye')