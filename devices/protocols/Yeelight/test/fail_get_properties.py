from yeelight import Bulb

bulb = Bulb('192.168.1.162')
import logging
logging.basicConfig(level=logging.DEBUG)

def print_props():
    print("=" * 80)
    print(bulb.get_properties())


bulb.turn_on()
print_props()


bulb.set_color_temp(4500)
print_props()

bulb.turn_off()
print_props()

