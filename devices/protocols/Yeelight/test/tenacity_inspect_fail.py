
import tenacity
import inspect


class FooError(Exception):pass


TENACITY_RETRY=tenacity.stop_after_attempt(2) | tenacity.stop_after_delay(10)

@tenacity.retry(stop=TENACITY_RETRY)
def foo(bar):
    print(bar)
    raise(FooError)


specs=inspect.getargspec(foo)
print(specs)
f = foo('bar')
