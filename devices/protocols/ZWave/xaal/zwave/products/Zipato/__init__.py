from xaal.zwave import products

from .RGBBulb import RGBBulb

products.register(RGBBulb)
