from .DSB09104 import DSB09104
from .ZW096 import ZW096
from xaal.zwave import products
products.register(DSB09104)
products.register(ZW096)