xaal.ipx800
===========
This package contains a xAAL gateway for IPX-800 Ethernet Control System.
We tested it with IPX-800 V3.


Usage
-----
To run the gateway:

- edit your config file (*ipx800.ini*)
- launch *python -m xaal.ipx800*


TODO
----
Due to time constraint, only channel outputs (relays) are supported right now.
You can add easily analog or digital inputs, everything is ready.
