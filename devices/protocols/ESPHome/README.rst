xaal.esphome
============
This package provides a simple way to integrate ESPHome devices with xAAL.


Usage
-----

.. code:: shell

  python -m xaal.esphome
  # or
  xaal-pkgrun esphome

- support Switch, Lamp and Contact right now
- more support still to come.
