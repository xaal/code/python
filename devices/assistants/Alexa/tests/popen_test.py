import asyncio

ALEXA_SCRIPT="alexa_remote_control.sh"

async def run(cmd):
    cmd = ALEXA_SCRIPT+ " " + cmd
    proc = await asyncio.create_subprocess_shell(cmd,stdout=asyncio.subprocess.PIPE,stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate()
    print(f'[{cmd!r} exited with {proc.returncode}]')
    if stdout:
        print(f'[stdout]\n{stdout.decode()}')
    if stderr:
        print(f'[stderr]\n{stderr.decode()}')


async def say(txt):
    await run('-e speak:"%s"' % txt)


#asyncio.run(run('alexa_remote_control.sh   -e speak:"test"'))