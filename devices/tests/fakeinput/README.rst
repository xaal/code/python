xaal.fakeinput
==============
This package provides some dummy (fake) input devices for the xAAL Project. It come with a web interface to control the devices.
We use this package to fake the lab inputs for testing purpose.

Usage
-----
Just edit the 'fakeinput.ini' file to fit your needs and run the package.

.. code-block:: bash

    $ python -m xaal.fakeinput
    # or
    $ xaal-pkgrun fakeinput

Open the web interface to control the devices (http://localhost:8081).
