import asyncio
import socket
import struct


from xaal.lib import MessageFactory, config, helpers

ADDR = config.address
PORT = config.port


class EchoServerProtocol(asyncio.Protocol):
    def __init__(self, on_con_lost):
        self.on_con_lost = on_con_lost
        self.mf = MessageFactory(config.key)
        helpers.setup_console_logger()

    def connection_made(self, transport):
        # import pdb;pdb.set_trace()
        sock = transport.get_extra_info('socket')
        (addr, port) = sock.getsockname()
        mreq = struct.pack('4sl', socket.inet_aton(addr), socket.INADDR_ANY)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 10)
        self.transport = transport

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

    def datagram_send(self, data):
        self.transport.sendto(data, (ADDR, PORT))

    def datagram_received(self, data, addr):
        # print('Received %r from %s' % (data, addr))
        msg = self.mf.decode_msg(data)
        if msg:
            msg.dump()


async def main():
    loop = asyncio.get_running_loop()

    print("Starting UDP server")
    on_con_lost = loop.create_future()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(on_con_lost), local_addr=(ADDR, PORT), reuse_port=True
    )

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


try:
    asyncio.run(main())
except KeyboardInterrupt:
    print("Bye Bye")
