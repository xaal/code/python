import asyncio
import socket
import struct
import uuid

ADDR = '224.0.29.200'
PORT = 1230


class EchoServerProtocol(asyncio.Protocol):
    def __init__(self, on_con_lost):
        # self.on_con_lost = on_con_lost
        pass

    def connection_made(self, transport):
        self.transport = transport

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        # self.on_con_lost.set_result(True)

    def datagram_send(self, data):
        self.transport.sendto(data, (ADDR, PORT))

    def datagram_received(self, data, addr):
        print('Received %r from %s' % (data, addr))


async def test_send(protocol, uid):
    while True:
        msg = f"msg from {uid}"
        protocol.datagram_send(msg.encode("utf-8"))
        await asyncio.sleep(5)


def new_sock():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    try:
        # Linux + MacOS + BSD
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    except:
        # Windows
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('0.0.0.0', PORT))
    mreq = struct.pack('4sl', socket.inet_aton(ADDR), socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 10)
    return sock


async def main():
    loop = asyncio.get_running_loop()
    print("Starting UDP server")
    on_con_lost = loop.create_future()

    transport, protocol = await loop.create_datagram_endpoint(lambda: EchoServerProtocol(on_con_lost), sock=new_sock())
    # transport, protocol = await loop.create_datagram_endpoint( lambda: EchoServerProtocol(on_con_lost), local_addr=(ADDR, PORT),reuse_port=True)
    asyncio.ensure_future(test_send(protocol, uuid.uuid1()))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Bye Bye")
