from xaal.lib import AsyncEngine, helpers
from xaal.lib.aioengine import console

import asyncio
import logging
import functools

helpers.setup_console_logger()
logger = logging.getLogger('test-timer')


async def sleep1():
    logger.warning('Sleep1 start')
    await asyncio.sleep(15)
    logger.warning('Sleep1 done')


async def sleep2():
    logger.warning('Sleep2 start')
    await asyncio.sleep(20)
    logger.warning('Sleep2 done')


async def _console(locals):
    await console(locals=locals)


async def test(foo, bar=None):
    print('=' * 78)
    print('Foo: %s' % foo)
    print('Bar: %s' % bar)


eng = AsyncEngine()
eng.add_timer(sleep1, 1, 1)
eng.add_timer(sleep2, 1, 1)
eng.on_start(test, 'Foo', bar=12)

ptr = functools.partial(_console, locals())
eng.add_timer(ptr, 1, 2)

# asyncio.ensure_future(console(locals()))

try:
    eng.run()
except KeyboardInterrupt:
    pass
