from xaal.lib import helpers


class Goo:
    def test(self, name):
        self.test.counter = getattr(self.test, 'counter', 0) + 1
        print(f"{name} => {self.test.counter}")


@helpers.static_vars(counter=0)
def test_func(name='Foo'):
    print(f"{name} => {test_func.counter}")
    test_func.counter += 1


test_func()
test_func('Bar')
test_func('Baz')

goo = Goo()
try:
    # static vars don't work with class methods
    goo.test('Goo')
except AttributeError as e:
    print(f"AttributeError: {e}")
