# from gevent import monkey
# monkey.patch_all()


def is_gevent_monkey_patched():
    try:
        from gevent import monkey
    except ImportError:
        print("gevent is not installed")
        return False
    else:
        return monkey.is_module_patched('__builtin__')


print(is_gevent_monkey_patched())
