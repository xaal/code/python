from xaal.lib import AsyncEngine, helpers
import functools

helpers.setup_console_logger()


def shutdown(eng):
    eng.shutdown()


eng = AsyncEngine()
ptr = functools.partial(shutdown, eng)
eng.add_timer(ptr, 2)
eng.run()
