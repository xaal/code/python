from xaal.lib import bindings, cbor

#import ipdb;ipdb.set_trace()

data = bindings.UUID.random()
tmp = cbor.dumps(data)
print(type(cbor.loads(tmp)))


"""
uuid = bindings.UUID.random()
url = bindings.URL("http://google.fr")

data = ["foobar",2,{"device":uuid, "url":url}]


tmp = cbor.dumps(data)
print(f"{data} => {tmp}")

dec = cbor.loads(tmp)
print(f"{tmp} => {dec}")

print(f"UUID: {type(dec[2]['device'])}")
print(f"URL: {type(dec[2]['url'])}")
"""