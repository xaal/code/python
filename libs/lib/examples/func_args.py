from xaal.lib import core, helpers
from xaal.lib import aiohelpers

import asyncio
import time
import logging

logger = logging.getLogger(__name__)
helpers.setup_console_logger()


def foo(arg0, arg1):
    print(f"{arg0} {arg1}")


async def goo(arg0, arg1):
    print(f"{arg0} {arg1}")


@aiohelpers.spawn
def bar(arg0, arg1):
    print(f"{arg0} {arg1}")
    logger.warning('sleep')
    time.sleep(5)
    logger.warning('eof sleep')


if __name__ == '__main__':
    logger.debug('start...')

    print(f" {core.get_args_method(foo)}")
    print(f" {core.get_args_method(goo)}")
    print(f" {core.get_args_method(bar)}")

    t1 = asyncio.ensure_future(goo('goo1', 'goo2'))
    # t2=asyncio.ensure_future(bar('goo1','goo2'))
    t2 = bar('goo1', 'goo2')

    futures = asyncio.wait([t1])

    logger.debug('Waiting..')
    asyncio.get_event_loop().run_until_complete(futures)
