""" 
Simple test to study the death the engine with a running thread.
"""

import asyncio
from xaal.lib import AsyncEngine, helpers
import time


@helpers.spawn
def test1(event):
    while not event.is_set():
        time.sleep(1)
        print('test1')


async def test2():
    while 1:
        await asyncio.sleep(1)
        print('test2')


eng = AsyncEngine()
print(eng.new_task(test2()))

ev = asyncio.Event()
test1(ev)
eng.on_stop(ev.set)
# hit ctrl-c now
eng.run()
