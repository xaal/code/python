class Foo:
    def __init__(self):
        self.goo = 'Goo'
        self.__bar = 'Bar'

    @property
    def bar(self):
        return self.__bar

    @bar.setter
    def bar(self, value):
        self.__bar = value

    def __setattr__(self, name, value):
        propobj = getattr(self.__class__, name, None)
        if isinstance(propobj, property):
            propobj.fset(self, value)
            return

        if name not in ['goo', '_Foo__bar', 'foo']:
            raise AttributeError(f'unknow attribute {name}')
        self.__dict__[name] = value

    def dump(self):
        print(self.__dict__)


a = Foo()
print(a.bar)
a.bar = 'boo'
a.goo = 'goo'
a.foo = 'foo'
try:
    a.baz = 'baz'
except AttributeError as e:
    print(e)

a.dump()
