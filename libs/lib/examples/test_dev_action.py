"""
Simple message encoding / decoding to test the search method
"""

from xaal.lib import MessageFactory, MessageType
from xaal.lib import config, core, helpers, engine
from xaal.schemas import devices

helpers.setup_console_logger()

mf = MessageFactory(config.key)
source = devices.hmi()
target = devices.lamp_color()

data = mf.build_msg(source, [target.address], MessageType.REQUEST, 'turn_off', {'smooth': 20})
msg = mf.decode_msg(data)
assert msg

print(f"Message: {msg}")
engine.run_action(msg, target)

method, params = core.search_action(msg, target)
print(f"Method found: {method} params {params} ")
