from xaal.lib import Engine
from xaal.lib.network import NetworkConnector
from xaal.lib.helpers import setup_console_logger
from xaal.schemas import devices
import functools
import asyncio
from decorator import decorator


@decorator
def spawn(func, *args, **kwargs):
    print(f"Calling {func.__name__}")
    asyncio.ensure_future(func(*args, **kwargs))


def spawn0(func):
    @functools.wraps(func)
    def spawn_future(*args, **kwargs):
        print(f"Calling {func.__name__}")
        asyncio.ensure_future(func(*args, **kwargs))

    return spawn_future


class AioNetworkConnector(NetworkConnector):

    async def receive(self):
        data = NetworkConnector.receive(self)
        # print(data)
        print("receive")
        return data


class AioEngine(Engine):
    async def run(self):
        self.start()
        self.running = True
        while self.running:
            self.loop()
            await asyncio.sleep(0)


def handler(data):
    # print(data)
    print("hanlder")


@spawn
async def toggle(dev):
    print("Toggle")
    await asyncio.sleep(2)
    dev.attributes['light'] = False if dev.attributes['light'] else True


@spawn
async def test(dev, _foo):
    print(f"test.... {dev} {_foo}")


async def foo():
    while 1:
        print("Foo")
        await asyncio.sleep(30)


async def test_receive():
    net = AioNetworkConnector('224.0.29.200', 1236, 10)
    net.connect()
    while True:
        await net.receive()


def main():
    setup_console_logger()
    # loop = asyncio.new_event_loop()
    eng = AioEngine()
    # eng.add_rx_handler(handler)

    from xaal.lib import tools

    dev = devices.lamp_toggle(tools.get_random_uuid())
    dev.info = 'FooBar'
    eng.add_device(dev)

    ptr = functools.partial(toggle, dev)
    dev.methods['toggle'] = ptr
    eng.add_timer(ptr, 10)

    ptr = functools.partial(test, dev)
    dev.methods['test'] = ptr

    tasks = [
        asyncio.ensure_future(eng.run()),
        #  asyncio.ensure_future(foo()),
        #  asyncio.ensure_future(test_receive()),
    ]

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait(tasks))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("Bye bye")
