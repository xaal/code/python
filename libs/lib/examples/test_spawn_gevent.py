from gevent import monkey

monkey.patch_all(thread=False)
import gevent
from xaal.lib import AsyncEngine, helpers

helpers.setup_console_logger()


def loop():
    while 1:
        print('loop')
        gevent.sleep(1)


eng = AsyncEngine()
gevent.spawn(loop)
eng.run()
