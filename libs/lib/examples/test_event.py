import asyncio

event = asyncio.Event()


async def test1():
    await event.wait()
    print('Test1')


async def test2():
    await event.wait()
    print('Test2')


async def run():
    print('running')
    await asyncio.sleep(5)
    event.set()
    print('event set')
    await asyncio.sleep(1)
    event.clear()


loop = asyncio.get_event_loop()

t1 = loop.create_task(test1())
t2 = loop.create_task(test2())
t3 = loop.create_task(run())

try:
    loop.run_forever()
except KeyboardInterrupt:
    print('done')
