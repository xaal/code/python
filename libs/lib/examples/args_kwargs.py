from xaal.lib.aioengine import Hook, HookType
import asyncio


async def test1(foo, bar=None):
    print('=' * 78)
    print('Foo: %s' % foo)
    print('Bar: %s' % bar)


async def test2(*args, **kwargs):
    print('=' * 78)
    print('Args: %s' % str(args))
    print('Kwargs: %s' % str(kwargs))


async def goo(func, *args, **kwargs):
    print("Args: %s" % args)
    print("kwarrgs: %s" % kwargs)
    h = Hook(HookType.start, func, *args, **kwargs)
    print('*' * 78)
    print("Args: %s" % h.args)
    print("kwarrgs: %s" % h.kwargs)
    await h.func(*h.args, **h.kwargs)


asyncio.run(goo(test1, 'Foo', bar=12))
asyncio.run(goo(test2, 'Foo', bar=12))
