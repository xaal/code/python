

class XAAL_${name} extends XAALDevice {

    setup() {
        this.devtype = '${devtype}';
    % for attr in attributes:  
        this.${attr} = null;
    % endfor
    }
    % for meth in methods:
<%
        keys = list(methods[meth].get('in',{}).keys())
        #tmp = map(lambda x: '_%s=None' % x,keys)
        args = ','.join(keys)
%>    ${meth}(${args}){
        console.log("Calling ${meth} methods");
    }

    % endfor
}