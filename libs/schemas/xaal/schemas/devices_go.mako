// =====================================================================
func New${Name}(addr uuid.UUID) *xaal.Device {
	dev := xaal.NewDevice("${devtype}")
	dev.Address = addr
	% if (len(attributes) !=0):

	// -------- Attributes --------
	% for attr in attributes:
<%
	dm = datamodel[attributes[attr]]
	desc = dm.get('description','no-desc ?')
	type_ = dm.get('type')
	unit = dm.get('unit')
	comment = desc
	if type_:
		comment = comment +' | type: ' + type_
	if unit:
		comment = comment +' | unit: ' + unit
%>	// ${comment}
<%
	type_ = dm.get('type')
	value = 'nil'
	if type_ == 'data = bool':
		value = 'false'
	elif type_ in ['data = uint',]:
		value = 0
	elif type_ in ['data = number',]:
		value = 0.0
	elif unit in ['K']:
		value = 0
	elif unit in ['%','%EL','%RH','°']:
		value = 0.0
%>	dev.AddAttribute("${attr}", ${value})
	% endfor
	% endif
	 % if (len(methods) !=0):

	// -------- Methods --------
	% for meth in methods:
<%
		keys = list(methods[meth].get('in',{}).keys())
		print_keys = ', '.join(keys)
		camel_meth = "default"+ f"{meth}".replace('_','.').title().replace('.','')

%>	// ${methods[meth]['description']}
		% if len(keys) != 0 :
	${camel_meth} := func(args xaal.MessageBody) *xaal.MessageBody {
		// Arguments: ${print_keys}
		slog.Info("${camel_meth} method: ${meth}", "args", args)
		% else:
	${camel_meth} := func(xaal.MessageBody) *xaal.MessageBody {
		slog.Info("${camel_meth} method: ${meth}")
		% endif
		return nil
	}
	% endfor
	% for meth in methods:
<%
		camel_meth = "default"+f"{meth}".replace('_','.').title().replace('.','')
%>	dev.AddMethod("${meth}", ${camel_meth})
	% endfor
	% endif

	return dev
}
