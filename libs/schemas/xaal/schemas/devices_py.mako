#=====================================================================
def ${name}(addr: Optional[UUID] = None) -> Device:
    """${doc}"""
    addr = addr or tools.get_random_uuid()
    dev = Device('${devtype}',addr)
    % if (len(attributes) !=0):
    
    # -- Attributes --
    % for attr in attributes:  
<% 
    dm = datamodel[attributes[attr]] 
    desc = dm.get('description','no-desc ?')
    type_ = dm.get('type')
    unit = dm.get('unit')
    comment = desc
    if type_:
        comment = comment +' | type: ' + type_
    if unit:
        comment = comment +' | unit: ' + unit

%>    # ${comment}
    dev.new_attribute('${attr}')
    % endfor
    % endif
    % if (len(methods) !=0):

    # -- Methods --
    % for meth in methods:
<%
        keys = list(methods[meth].get('in',{}).keys())
        tmp = map(lambda x: '_%s=None' % x,keys)
        args = ','.join(tmp)
 
        print_keys = ','.join(map(lambda x: '_%s' % x,keys))

        buf= ""
        for k in keys:
            buf=buf+"%s=[%%s]" % k+","
%>    def default_${meth}(${args}):
        """${methods[meth]['description']}"""
        % if len(args) == 0:
        logger.warning("default_${meth}()")
        % else:
        logger.warning("default_${meth}(${buf})" % (${print_keys}))
        % endif
        
    % endfor
    % for meth in methods:
    dev.add_method('${meth}',default_${meth})
    % endfor
    
    % endif
    return dev
