xaal.legacytools
================
This is a collection of tools for xAAL Bus. It is a legacy version of the tools that are now part of the xAAL project.
Checkout https://pypi.org/project/xaal.tools/ for the latest version.

This sets of tools provides the same functionalities but use the a much simpler API (no asyncio). This is provided for
compatibility with older systems and learning purposes.
