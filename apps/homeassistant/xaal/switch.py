import logging

from homeassistant.config_entries import ConfigEntry
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.components.switch import SwitchEntity, SwitchDeviceClass

from .bridge import XAALEntity, async_setup_factory

_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass: HomeAssistant,
                            config_entry: ConfigEntry,
                            async_add_entities: AddEntitiesCallback) -> None:

    binding = {'powerrelay.': [PowerRelay]}
    return async_setup_factory(hass, config_entry, async_add_entities, binding)


class PowerRelay(XAALEntity, SwitchEntity):
    _attr_device_class = SwitchDeviceClass.OUTLET

    @property
    def is_on(self) -> bool | None:
        return self.get_attribute('power')

    def turn_on(self, **kwargs) -> None:
        _LOGGER.debug(f"turn_on: {kwargs}")
        self.send_request('turn_on')

    def turn_off(self, **kwargs) -> None:
        self.send_request('turn_off')

