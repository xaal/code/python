xaal.dashboard
==============
This is a dashboard for the xAAL project. It is a web application that displays the current status of devices. Its main purpose is to
build a nice and easy to use interface to monitor the xAAL network. We used this dashboard to monitor the xAAL network during a large
number of demos.

This dashboard is somehow deprecated because the project provide a Home Assistant integration that is more powerful and more flexible.
However, this dashboard is still useful for people who want to have a quick and easy to use interface to monitor their xAAL network.

Usage
-----
To start the dashboard, you need to have a xAAL network running. You can start the dashboard with the following command:

  .. code:: shell

    python -m xaal.dashboard
    # or
    xaal-pkgrun dashboard
