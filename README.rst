
=================
xAAL Python stack
=================

Requirements
============
.. sectnum::

To install xAAL for Python, you need Python-3 (version 3.8 or above). And limited stack exists
for Python-2, you can find more informations `here <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/py2-backport>`_.

Un*x like
~~~~~~~~~
Install the following packages:

- subversion
- python3-dev
- python3-setuptools
- libsodium-dev

For Debian / Ubuntu users:

.. code-block:: bash

   $ apt-get install subversion python3-dev libsodium-dev python3-setuptools gcc
   # recommended
   $ apt-get install python3-venv python3-pip


Windows
~~~~~~~
This code can be used w/ Windows (tested w/ Win10 and Python 3.8). You can build
the virtualenv with the same way. To activate the venv, you should run Scripts\activate.bat.
For Powershell users (recommanded), you must tweak the ExecutionPolicy to be able to run
*Activate.ps1*.

  .. code-block::

      Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

To install libsodium. You can dowload it here: https://download.libsodium.org/libsodium/releases/
Extract the DLL from the zip file (libsodium-x.y.z-stable-msvc.zip\libsodium\x64\Release\vzzz\dynamic)
in the Win32 DLL folder.

If everything is fine, you can use exactly the same commands on Un*x and Windows. The xaal-* commands
are just renamed with an exe extension (ie: xaal-dumper.exe)

**Issues:** Right now some gateways won't work on Windows due to missing libaries (openZwave ie)


Installation
=============

Manual
~~~~~~
Right now, there is no public release (pip based) of xAAL Python binding, so
you have to install things from SVN or archive.

You can use virtualenv (recommended).

First build a virtualenv :

.. code-block:: bash

   $ python3 -m venv xaal_env


Everytime, you want to use the binding, you must source the activate script.

.. code-block:: bash

  $ source xaal_env/bin/activate


Download sources from SVN:

.. code-block:: bash

   $ git clone https://gitlab.imt-atlantique.fr/xaal/code/python.git xaal_git

First, install the xaal.lib package:

.. code-block:: bash

   $ cd xaal_git/libs/lib/
   $ pip install -e .

Install the monitor lib (needed by Dashboard, REST API..)

.. code-block:: bash

   $ cd xaal_git/libs/monitor/
   $ pip install -e .

Install the schemas (needed by some devices)

.. code-block:: bash

   $ cd xaal_git/libs/schemas/
   $ pip install -e .


Install the tools

.. code-block:: bash

   $ cd xaal_git/apps/tools
   $ pip install -e .

You can remove the *-e* in pip install command , but modification in source files
 won't be applied, you have to re-install it. Right now, *-e* is the best option.

Create the configuration file in your home directory:

.. code-block:: bash

   $ mkdir ~/.xaal/
   $ cp xaal_git/libs/lib/xaal.ini.sample ~/.xaal/xaal.ini
   $ xaal-keygen

xaal-keygen will compute an key for a given passphrase. Edit the xaal.ini
file according to your needs.


Automatic
~~~~~~~~~

Instead of manual installation of each package, you can use the *install.py* script.
This script will install allmost all packages without user interaction.


Docker
~~~~~~
The docker folder contains an Dockerfile to run the xALL software in a docker
container. The Dockerfile use the automatic installation script.


Tests
=====
First, you can launch a message dumper with this tools

.. code-block:: bash

   $ xaal-dumper
   $ or xaal-tail

To start an fake lamp:

.. code-block:: bash

   $ python -m xaal.dummy.lamp

To check devices, you can use:

.. code-block:: bash

   # search alive devices
   $ xaal-isalive

   # search lamp.basic devices
   $ xaal-isalive -t lamp.basic

   # search any kind of lamp
   $ xaal-isalive -t lamp.any

   # display description / attribute
   $ xaal-info xxxxxxxxxxxxxx <- uuid

   # display description / attribute for all devices
   $ xaal-walker

   # same but only for lamp devices
   $ xaal-walker -t lamp.any

To turn on/off the lamp

.. code-block:: bash

   # turn on the lamp
   $ xaal-send -d xxxxxxxxxxxxxx -r turn_off
   $ xaal-send -d xxxxxxxxxxxxxx -r turn_on


Informations
============

Coding style
~~~~~~~~~~~~
Every xAAL package (device, gateway, app) use a namespace. For example, *xaal.rest*,
or *xaal.zwave*. By convention, you can run everything just by calling the namespace
module.

.. code-block:: bash

   # to run the meta data sever:
   $ python -m xaal.metadb

   # to run the dashboard (web interface)
   $ python -m xaal.dashboard

   # to run the Open Weather Map gateway
   $ python -m xaal.owm

To reduce memory footprint, you can use the *xaal-pkgrun* command. This tool will
register every package on a single Python interpreter.

.. code-block:: bash

   # to run aqara and owm gateways:
   $ xaal-pkgrun aqara owm


Links
~~~~~
- The repository contains a large number of devices, gateways and tools. You can find
  the `packages list here <packages.rst>`_.

- You can find the `xAAL lib documentation here. <./libs/lib/README.rst>`_


Notes
~~~~~
- If you use xAAL on multiple hosts, take care of the system clock. xAAL use
  date/time to cypher the messages. If clocks differs, you will receive an error
  message about a "replay attack". In production, NTP is your best friend. A window
  of 1 minutes is accepted, but no more.


FAQ
~~~
- Configuration files are hard to read / edit. Why don't you use YAML or XML
  for config ?

  First, we need something that support nested config so we can not use
  ConfigParser. Next, we tested severals YAML packages, but they are really
  slow to import. We want xAAL stack to load as fast as possible, and importing
  big packages (like PyYAML) take around 0.5 sec on a Raspy. This is way too
  much for a simple command-line tools like xaal-info for example.
  We want to provide a better user experience.
