from xaal.lib import Engine, tools
from xaal.schemas import devices
from xaal.monitor import Monitor
import platform

PKG_NAME = "btn_relay_labo"


def UUID(uuids):
    r = []
    for k in uuids:
        r.append(tools.get_uuid(k))
    return r


LAMPS = UUID(
    [
        "ccc44227-d4fc-46eb-8578-159e2c47da03",
        "ccc44227-d4fc-46eb-8578-159e2c47da04",
        "ccc44227-d4fc-46eb-8578-159e2c47da05",
        "ccc44227-d4fc-46eb-8578-159e2c47da06",
        "ccc44227-d4fc-46eb-8578-159e2c47da07",
        "ccc44227-d4fc-46eb-8578-159e2c47da08",
    ]
)

SPOTS = UUID(["6265eb30-8c59-11e9-98b1-b827ebe99201"])
AMBI = UUID(["e19d5ea8-c838-11ea-82a8-9cebe88e1963"])


# Edisio labo
BTNS = UUID(
    [
        "743034ca-c2f0-11e8-9485-a40074bcb601",
        "743034ca-c2f0-11e8-9485-a40074bcb603",
        "743034ca-c2f0-11e8-9485-a40074bcb605",
        "743034ca-c2f0-11e8-9485-a40074bcb607",
        "743034ca-c2f0-11e8-9485-a40074bcb608",
    ]
)

# Aquara sw1
AQUARA = UUID(["1ec6bbd0-20b5-11e9-b352-a4badbf92500"])
SDB = UUID(
    [
        "fb1f8648-20ba-11e9-b352-a4badbf92500",
        "fb1f8648-20ba-11e9-b352-a4badbf92501",
        "fb1f8648-20ba-11e9-b352-a4badbf92502",
    ]
)
SCEN = UUID(
    [
        "00796898-20bb-11e9-b352-a4badbf92500",
        "00796898-20bb-11e9-b352-a4badbf92501",
        "00796898-20bb-11e9-b352-a4badbf92502",
    ]
)


BTN_LUM = UUID(
    [
        "ee220704-e54d-11eb-9dbb-a4badbf92500",
        "ee249546-e54d-11eb-9dbb-a4badbf92500",
        "ee25b43a-e54d-11eb-9dbb-a4badbf92500",
        "ee25238a-e54d-11eb-9dbb-a4badbf92500",
        "ee22bfaa-e54d-11eb-9dbb-a4badbf92500",
    ]
)
BTN_SHUT = UUID(
    [
        "ee23caee-e54d-11eb-9dbb-a4badbf92500",
        "ee23caee-e54d-11eb-9dbb-a4badbf92501",
        "ee23caee-e54d-11eb-9dbb-a4badbf92502",
    ]
)


BTN_SCREEN = UUID(["ee22bfaa-e54d-11eb-9dbb-a4badbf92501"])
SCREEN_POWER = UUID(["2d42a742-aa2f-11e9-ac3b-a4badbf92501"])

# Edisio bureau
# BTNS = ['c4b33536-c314-11e8-a5d6-000673742b01','c4b33536-c314-11e8-a5d6-000673742b03','c4b33536-c314-11e8-a5d6-000673742b05',
#        'c4b33536-c314-11e8-a5d6-000673742b07','c4b33536-c314-11e8-a5d6-000673742b08']


SHUTERS = UUID(
    ["e4b05165-be5d-46d5-acd0-4da7be1158ed", "2fe70f46-3ece-44d1-af34-2d82e10fb854"]
)
SIRENS = UUID(["980a639c-20b1-11e9-8d70-a4badbf92501"])

LAMPS_A = [LAMPS[0], LAMPS[2], LAMPS[5]]
LAMPS_B = [LAMPS[1], LAMPS[3], LAMPS[4]]

mon = None


def send(targets, action, body=None):
    global mon
    engine = mon.dev.engine
    print(f"{targets} {action}")
    engine.send_request(dev, targets, action, body)


def search_for_light(lamps):
    for l in lamps:
        dev = mon.devices.get_with_addr(l)
        if dev:
            light = dev.attributes.get("light", None)
            if light:
                return True
    return False


def on_off_light(lamps):
    if search_for_light(lamps):
        send(lamps, "turn_off")
        return False
    else:
        send(lamps, "turn_on")
        return True


def handle_msg(msg):
    if not msg.is_notify():
        return
    # search for the buttons
    if msg.action == "click":
        if msg.source == BTNS[0]:
            on_off_light(LAMPS_A)

        if msg.source == BTNS[1]:
            on_off_light(LAMPS_B)

        # btn du milieu / on éteint tout
        if msg.source == BTNS[2]:
            ALL = LAMPS + SPOTS + AMBI
            r = search_for_light(ALL)
            if r:
                send(ALL, "turn_off")
            else:
                send(LAMPS, "turn_on")

        # Eclairage d'ambiance
        if msg.source == BTNS[3]:
            on_off_light(SPOTS + AMBI)

        if msg.source in AQUARA:
            if on_off_light(LAMPS):
                # send([SHUTERS[0],],'up')
                # send([SHUTERS[1],],'up')
                pass
            else:
                # send([SHUTERS[0],],'down')
                # send([SHUTERS[1],],'down')
                pass

        if msg.source == SDB[0]:
            send([LAMPS[3]], "toggle")

        if msg.source == SCEN[0]:
            # "soirée TV"
            send(SCREEN_POWER, "turn_on")
            send([SHUTERS[0]], "down")
            send(LAMPS, "turn_off")
            send(SPOTS + AMBI, "turn_on")
            send([SHUTERS[1]], "down")

        if msg.source == SCEN[1]:
            # "Début journée"
            send([SHUTERS[0]], "up")
            send(
                [
                    LAMPS[3],
                ],
                "turn_on",
            )
            send(SPOTS + AMBI, "turn_off")
            send([SHUTERS[1]], "up")

        if msg.source == SCEN[2]:
            # "stop"
            send([SHUTERS[0]], "stop")
            send([SHUTERS[1]], "stop")
            send(SIRENS, "stop")

        # ========= Commande éclairage séparé =====
        if msg.source == BTN_LUM[0]:
            on_off_light([LAMPS[0]])

        if msg.source == BTN_LUM[1]:
            on_off_light([LAMPS[1]])

        if msg.source == BTN_LUM[2]:
            on_off_light([LAMPS[2]])

        if msg.source == BTN_LUM[3]:
            on_off_light([LAMPS[4]])

        if msg.source == BTN_LUM[4]:
            on_off_light([LAMPS[5]])

        # ======== Commandes des volets =========
        if msg.source == BTN_SHUT[0]:
            send(SHUTERS, "up")

        if msg.source == BTN_SHUT[1]:
            send(SHUTERS, "down")

        if msg.source == BTN_SHUT[2]:
            send(SHUTERS, "stop")

        # ======= TV
        if msg.source in BTN_SCREEN:
            tv = mon.devices.get_with_addr(SCREEN_POWER[0])
            if tv:
                state = tv.attributes.get("power", None)
                if state == True:
                    send(SCREEN_POWER, "turn_off")
                elif state == False:
                    send(SCREEN_POWER, "turn_on")
                else:
                    print("Unkown state for main screen")

    if msg.action == "double_click":
        if msg.source in AQUARA:
            send([SHUTERS[0]], "stop")
            send([SHUTERS[1]], "stop")
            send(SIRENS, "stop")

    if msg.action == "attributesChange":
        # print(msg)
        if msg.source == tools.get_uuid("d9993018-20b1-11e9-a250-a4badbf92500"):
            pres = msg.body.get("presence")
            light = mon.devices.get_with_addr(LAMPS[3]).attributes.get("light")
            print("pres & light %s/%s" % (pres, light))
            if not pres and light:
                send(SIRENS, "play")
                print("Alarme..")


def main():
    global dev
    global mon
    dev = devices.basic()
    dev.info = "%s@%s" % (PKG_NAME, platform.node())
    engine = Engine()
    engine.add_device(dev)
    engine.subscribe(handle_msg)
    mon = Monitor(dev)
    engine.run()


if __name__ == "__main__":
    try:
        eng = main()
    except KeyboardInterrupt:
        print("Bye bye")
