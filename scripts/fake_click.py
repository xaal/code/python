from xaal.lib import Engine,tools,MessageType
from xaal.schemas import devices
import sys

eng = Engine()
mf = eng.msg_factory

uuid = tools.get_uuid(sys.argv[1])

dev = devices.button(uuid)
msg = mf.build_msg(dev,[],MessageType.NOTIFY,action='click')
eng.send_msg(msg)
eng.loop()
                    
