from xaal.lib import Engine,tools
from xaal.schemas import devices
from xaal.monitor import Monitor

import platform
UUID = tools.get_uuid

# device address, no config file here.
# this is just for testing.
ADDR = UUID('aa16f008-5db4-11ec-a53d-d6bd5fe18736')
# devices uses in this scene
TTS_ALEXA = UUID('c968a442-1d59-11ec-90a5-d6bd5fe18701')
GARAGE    = UUID('0cbcf21c-92b0-11e8-80cd-408d5c18c800')
OUTDOOR   = UUID( '5fcc6ad1-804d-49cb-b66c-e877f2374900')
INDOOR    = UUID('43c4a724-055f-11e9-8716-82ed25e6aa00')

TEMP = [GARAGE,OUTDOOR,INDOOR]

def filter(msg):
    if msg.source in TEMP:
        return True
    return False

def get_temp(mon,addr):
    dev = mon.devices.get_with_addr(addr)
    if dev:
        value = dev.attributes.get('temperature',None)
        if value:
            return value
    return None


def main():
    engine = Engine()
    engine.start()

    dev = devices.scenario(ADDR)
    dev.vendor_id  = 'IHSEV'
    dev.product_id = 'Simple scenario to test Alexa' 
    dev.info       = 'Alexa temperature scene@%s' % (platform.node())
    dev.attributes['enabled'] = True

    engine.add_device(dev)
    mon  = Monitor(dev,filter_func=filter)

    def run():
        txt = ''
        # Alexa prefer when we use full phrases.
        tmp = get_temp(mon,OUTDOOR)
        if tmp:
            txt += 'la température extérieure est de %s°, ' % tmp

        tmp = get_temp(mon,GARAGE)
        if tmp:
            txt += 'la température du garage est de %s°, ' % tmp

        tmp = get_temp(mon,INDOOR)
        if tmp:
            txt += 'La température du salon est de %s°.' % tmp

        engine.send_request(dev,[TTS_ALEXA],'say',{'msg':txt})

    dev.methods['run'] = run
    # no abort here, cause Alexa don't support this.
    #dev.methods['abort'] = lambda: None
    engine.run()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nBye!")