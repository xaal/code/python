#!/usr/bin/env python

from xaal.lib import NetworkConnector
import socket
import select
import argparse
import hashlib
import collections

class UDPConnector(object):
    UDP_MAX_SIZE = 65507

    def __init__(self,addr,port):
        self.addr = addr
        self.port = port

    def connect(self):
        self.__sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

    def send(self,data):
        self.__sock.sendto(data,(self.addr,self.port))
    
    def receive(self):
        packt = self.__sock.recv(self.UDP_MAX_SIZE)
        return packt

    def get_data(self):
        r = select.select([self.__sock, ], [], [], 0.02)
        if r[0]:
            return self.receive()
        return None

def run(u_addr,u_port,m_addr,m_port):
    udp = UDPConnector(u_addr,u_port)
    udp.connect()
    #udp.send(b'hello')

    mc = NetworkConnector(m_addr,m_port,10)
    mc.connect()

    queue = collections.deque(maxlen=50)

    while 1:
        data = udp.get_data()
        if data:
            h = hashlib.blake2b(data).hexdigest()
            queue.append(h)
            mc.send(data)
           
        data = mc.get_data()
        if data:
            h=hashlib.blake2b(data).hexdigest()
            if h not in queue:
                udp.send(data)

def main():
    ap = argparse.ArgumentParser(description="Simple un-reflector")
    ap.add_argument('-a',action='store',dest='mcast_addr',required=True)
    ap.add_argument('-p',type=int,action='store',dest='mcast_port',required=True)
    ap.add_argument('-u',action='store',dest='udp_addr',required=True)
    ap.add_argument('-r',type=int,action='store',dest='udp_port',required=True)
    args = ap.parse_args()
    run(args.udp_addr,args.udp_port,args.mcast_addr,args.mcast_port)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("Bye bye")
    