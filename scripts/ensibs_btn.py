from xaal.lib import Engine
from xaal.schemas import devices
from xaal.monitor import Monitor
import platform
from functools import partial

PKG_NAME = 'scenario_btn_ensibs'

LAMPS=['93e09003-708e-11e8-956e-00fec8f7138c','aa8cd2e4-8c5d-11e9-b0ba-b827ebe99201','93e09009-708e-11e8-956e-00fec8f7138c','93e09010-708e-11e8-956e-00fec8f7138c']

MEUBLE_CUIS_UP      = ['93e09080-708e-11e8-956e-00fec8f7138c',]
MEUBLE_CUIS_DOWN    = ['93e09081-708e-11e8-956e-00fec8f7138c',]
PLAN_TRAV_CUIS_UP   = ['93e09082-708e-11e8-956e-00fec8f7138c',]
PLAN_TRAV_CUIS_DOWN = ['93e09083-708e-11e8-956e-00fec8f7138c',]

PRISES_CONNECTEE = ['6265eb30-8c59-11e9-98b1-b827ebe99201',]

# Edisio labo
EDISIO=['43c06446-8c5c-11e9-a105-b80673742b01','43c06446-8c5c-11e9-a105-b80673742b03','43c06446-8c5c-11e9-a105-b80673742b05',
        '43c06446-8c5c-11e9-a105-b80673742b07','43c06446-8c5c-11e9-a105-b80673742b08']

# Aquara sw1
AQUARA_1=['fb1f8648-20ba-11e9-b352-a4badbf92500','fb1f8648-20ba-11e9-b352-a4badbf92501','fb1f8648-20ba-11e9-b352-a4badbf92502']
AQUARA_2=['00796898-20bb-11e9-b352-a4badbf92500','00796898-20bb-11e9-b352-a4badbf92501','00796898-20bb-11e9-b352-a4badbf92502']
AQUARA_R= '1ec6bbd0-20b5-11e9-b352-a4badbf92500'

SHUTERS=['93e09051-708e-11e8-956e-00fec8f7138c','93e09050-708e-11e8-956e-00fec8f7138c']

SIREN= ['980a639c-20b1-11e9-8d70-a4badbf92501',]
#PUSH_BULLET=['bc5bb184-8d16-11e9-b4db-9cebe88e1963']
BULLET= ['6eb64b73-6e51-11e9-8f96-00fec8f7138c',]

LAMPS_A = [LAMPS[0],LAMPS[1]]

BLINKS = ['aa8cd2e4-8c5d-11e9-b0ba-b827ebe99201','980a639c-20b1-11e9-8d70-a4badbf92500']

#RELAYS = ['d34fd5be-8c58-11e9-8999-b827ebe99201']

mon = None
dev = None

def send(targets,action,body=None):
    global dev
    print("Sending %s %s" % (targets,action))
    dev.engine.send_request(dev,targets,action,body)

def delayed_send(delay,targets,action,body=None):
    global dev
    engine = dev.engine
    func = partial(send,targets=targets,action=action,body=None)
    engine.add_timer(func,delay,1)
    
def search_for_light(lamps):
    for l in lamps:
        dev = mon.devices.get_with_addr(l)
        if dev:
            light = dev.attributes.get('light',None)
            if light:
                return True
    return False

def on_off_light(lamps):
    print('on_off %s' % lamps)
    if search_for_light(lamps):
        send(lamps,'off')
        return False
    else:
        send(lamps,'on')
        return True

def siren_play(snd=2):
    send(SIREN,'play',{'sound':snd})

def siren_stop():
    send(SIREN,'stop')

#def pushbullet(title,msg):
    #send(BULLET,'notify',{'title': title,'msg':msg})
def pushbullet():
    send(BULLET,'notify',{'title':'Alerte','msg':"Appel secour"})

def blink():
    send(BLINKS,'blink')


def handle_msg(msg):
    if not msg.is_notify():
        return
    # search for the buttons 
    if msg.action == 'click':
        if msg.source == AQUARA_R:
            blink()
            siren_play(2)
            #pushbullet('Alerte','')
            pushbullet()

        if msg.source == AQUARA_1[0]:
            send(MEUBLE_CUIS_DOWN,'on')
            delayed_send(6,MEUBLE_CUIS_DOWN,'off')
            send(PLAN_TRAV_CUIS_DOWN,'on')
            delayed_send(8,PLAN_TRAV_CUIS_DOWN,'off')
        
        if msg.source == AQUARA_1[1]:
            #on_off_light(LAMPS_A)
            #send(RELAYS,'on')
            #on_off_light(LAMPS)
            send(MEUBLE_CUIS_UP,'on')
            delayed_send(6,MEUBLE_CUIS_UP,'off')
            send(PLAN_TRAV_CUIS_UP,'on')
            delayed_send(8,PLAN_TRAV_CUIS_UP,'off')
        
        if msg.source == AQUARA_1[2]:
            send(MEUBLE_CUIS_DOWN,'off')
            send(PLAN_TRAV_CUIS_DOWN,'off')
            send(MEUBLE_CUIS_UP,'off')
            send(PLAN_TRAV_CUIS_UP,'off')
            
        if msg.source == AQUARA_2[0]:
            #send(LAMPS,'on')
            delayed_send(5,[LAMPS[0],],'on')
            delayed_send(5,[LAMPS[3],],'on')
            delayed_send(10,[LAMPS[1],],'on')
            delayed_send(10,[LAMPS[2],],'on')
            send(SHUTERS,'down')
            #delayed_send(10,SHUTERS,'stop')
            delayed_send(15,PRISES_CONNECTEE,'on')

        if msg.source == AQUARA_2[1]:
            send(SHUTERS,'up')
            #send(LAMPS,'off')
            #delayed_send(5,LAMPS,'off')
            delayed_send(20,[LAMPS[1],],'off')
            delayed_send(20,[LAMPS[2],],'off')
            delayed_send(25,[LAMPS[0],],'off')
            delayed_send(25,[LAMPS[3],],'off')
            delayed_send(15,PRISES_CONNECTEE,'off')

        if msg.source == AQUARA_2[2]:
            send(SHUTERS,'stop')
        
        if msg.source == EDISIO[0]:
            send([SHUTERS[0],],'up')
        
        if msg.source == EDISIO[1]:
            send([SHUTERS[1],],'up')
        
        if msg.source == EDISIO[2]:
            send(SHUTERS,'stop')
        
        if msg.source == EDISIO[3]:
            send([SHUTERS[0],],'down')
        
        if msg.source == EDISIO[4]:
            send([SHUTERS[1],],'down')

                
def main():
    global mon,dev
    dev = devices.basic()
    dev.info = '%s@%s' % (PKG_NAME,platform.node())
    engine = Engine()
    engine.add_device(dev)
    engine.subscribe(handle_msg)
    mon = Monitor(dev)
    engine.run()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Bye bye')
