from xaal.lib import Device,Engine,tools,Message,helpers
from xaal.monitor import Monitor,Notification
import platform
import time
import logging
from enum import Enum


DELAY = 30

ADDR = 'aa4d1cbc-92af-11e8-80cd-408d5c18c800'
PKG_NAME = 'scenario_ensibs_alerte'

DOOR   = 'cbdb198c-20b1-11e9-a250-a4badbf92500'
MVT    = '93e09031-708e-11e8-956e-00fec8f7138c'
LIGHT  = '93e09005-708e-11e8-956e-00fec8f7138c'

BULLET= ['6eb64b73-6e51-11e9-8f96-00fec8f7138c']
BLINKS = ['aa8cd2e4-8c5d-11e9-b0ba-b827ebe99201','980a639c-20b1-11e9-8d70-a4badbf92500']
SIREN = ['980a639c-20b1-11e9-8d70-a4badbf92501',]


MONITORING_DEVICES = [DOOR,LIGHT,MVT]

class States(Enum):
    free       = 'free'
    busy       = 'busy'
    fail       = 'fail'
    no_motion  = 'no_motion'
    alarm      = 'alarm'


class Devices:
    def __init__(self):
        self.door = None
        self.light = None
        self.motion = None

    @property
    def list(self):
        return [self.door,self.light,self.motion]

    def check(self):
        for k in self.list:
            if k == None: return False
            if len(k.attributes.keys()) == 0: return False
        return True
    
    def used(self,dev):
        for k in self.list:
            if k==dev: return True
        return False

logger = logging.getLogger(PKG_NAME)
device = None
devices = Devices()
state = States.fail
motion_timer = 0 

def send(targets,action,body=None):
    global device
    device.engine.send_request(device,targets,action,body)

def alert():
    logger.warning('WARNING !!!!')
    send(BULLET,'notify',{'title':'Alarme WC haut !!','msg':"Personne inerte"})
    send(BLINKS,'blink')
    send(SIREN,'play')


def update_state():
    global state,device
    device.attributes['state'] = state.value


def on_event(event,dev):
    global state,motion_timer
    if event == Notification.new_device:
        if dev.address == DOOR: devices.door   = dev
        if dev.address == MVT : devices.motion = dev
        if dev.address == LIGHT: devices.light = dev

    if event == Notification.attribute_change:
        if devices.check() == False:
            state = States.fail
            update_state()
            return

        if state == States.fail:
            state = States.free
            update_state()

        logger.info(dev.attributes)
        if dev == devices.door:
            # close the door
            if dev.attributes['position'] == False:
                # mvt + light => busy 
                if state == States.free and devices.motion.attributes['presence'] == True and devices.light.attributes['light'] == True:
                    state = States.busy
                # no light => free
                if devices.light.attributes['light'] == False:
                    state = States.free
            else:
                # somebody open the door
                if state in [States.busy,States.alarm]:
                    state = States.free

        if dev == devices.motion:
            # no motion while busy => start timer
            if dev.attributes['presence']==False and state == States.busy:
                motion_timer = time.time()
                state = States.no_motion
            # motion while state no_motion => just busy
            if dev.attributes['presence'] == True and state == States.no_motion:
                state = States.busy
        update_state()


def update():
    global state,motion_timer,device
    now = time.time()
    if state == States.no_motion:
        if now > (motion_timer + DELAY):
            logger.warning('ALARME !!!')
            alert()
            state = States.alarm
            device.attributes['state'] = state.value


def filter_msg(msg):
    if msg.source in MONITORING_DEVICES:
        return True
    return False


def main():
    global mon,device
    device = Device('scenario.basic',ADDR)
    device.new_attribute('state')
    device.info = '%s@%s' % (PKG_NAME,platform.node())
    engine = Engine()
    engine.add_device(device)
    engine.add_timer(update,1)
    mon = Monitor(device,filter_func = filter_msg)
    mon.subscribe(on_event)
    engine.run()

if __name__ == '__main__':
    try:
        helpers.setup_console_logger()
        main()
    except KeyboardInterrupt:
        print('Bye bye')
        
