# dump the content of the metadb.ini file

from xaal.lib import tools
import sys

FILE= sys.argv[1]

cfg = tools.load_cfg_file(FILE)
if cfg == None:
    print("Unable to load %s" % FILE)
else:
    for dev in cfg['devices']:
        print(dev,end=' ')
        print(cfg['devices'][dev])