import asyncio
import time
import curses
from tabulate import tabulate

from xaal.lib import AsyncEngine, Message, MessageAction

FMT = "rounded_grid"

MSG_STATS = {
    "total": 0,
    "alive": 0,
    "request_isalive": 0,
    "request": 0,
    "reply": 0,
    "get_description": 0,
    "get_attributes": 0,
    "attributes_change": 0,
    "notify": 0,
}

DEVICE_STATS = {}


def count(msg: Message):
    msg_count(msg)
    device_count(msg)


def device_count(msg: Message):
    addr = msg.source
    try:
        DEVICE_STATS[addr] += 1
    except KeyError:
        DEVICE_STATS[addr] = 1


def msg_count(msg: Message):
    # Count the number of messages received
    MSG_STATS["total"] += 1
    if msg.is_request_isalive():
        MSG_STATS["request_isalive"] += 1
        return
    if msg.is_alive():
        MSG_STATS["alive"] += 1
        return
    if msg.is_reply() or msg.is_request():
        # This msg is a get attribute or description type
        if msg.action == MessageAction.GET_ATTRIBUTES.value:
            MSG_STATS["get_attributes"] += 1
            return
        if msg.action == MessageAction.GET_DESCRIPTION.value:
            MSG_STATS["get_description"] += 1
            return
    if msg.is_request():
        # direct request / not isalive / not attributes / description
        MSG_STATS["request"] += 1
    if msg.is_reply():
        MSG_STATS["reply"] += 1
    if msg.is_attributes_change():
        MSG_STATS["attributes_change"] += 1
        return
    if msg.is_notify():
        MSG_STATS["notify"] += 1


async def on_start():
    screen = curses.initscr()
    curses.noecho()
    curses.cbreak()

    curses.curs_set(0)
    screen.clear()

    t0 = time.time()
    while 1:
        screen.clear()
        t = time.time() - t0
        screen.addstr(1, 1, f"xAAL Messages stats since {time.ctime(t0)}")
        r = []
        for k, v in MSG_STATS.items():
            # count the nb of messages / s
            cnt = round(v / t, 1)
            r.append([k, v, cnt])
        data = tabulate(r, headers=["MSG Type", "Total", "Rate"], tablefmt=FMT)
        for i, line in enumerate(data.splitlines()):
            screen.addstr(3 + i, 1, line)

        sort = sorted(DEVICE_STATS.items(), key=lambda x: x[1], reverse=True)[0:10]
        # display the 10 most active devices, if any
        r = []
        for k, v in sort:
            r.append([k, v])
        data = tabulate(r, headers=["Device ADDR", "Total", "Rate"], tablefmt=FMT)
        for i, line in enumerate(data.splitlines()):
            screen.addstr(3 + i, 50, line)
        screen.refresh()
        await asyncio.sleep(5)


def on_stop():
    curses.echo()
    curses.nocbreak()
    curses.endwin()


def run():
    eng = AsyncEngine()
    eng.disable_msg_filter()
    eng.subscribe(count)

    eng.on_start(on_start)
    eng.on_stop(on_stop)
    eng.run()


if __name__ == "__main__":
    run()
