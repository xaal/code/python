"""
Bulk script to install all xAAL packages, use w/ caution.
"""


import glob
import os
import sys

PYTHON = sys.executable

BASE = ['./libs/lib', './libs/monitor', './libs/schemas']
BLACKLIST = ['lm_sensors',
             'gtk-notify',
             'conky',
             'ZWave',
             'SensFloor',
             'fuse']


def title(msg):
    print("="*78)
    print("Running %s" % msg)
    print("="*78)


def required():
    #os.system('pip install --upgrade pip')
    os.system('pip install uv')
    os.system('uv pip install wheel')


def setup_develop(path):
    title(path)
    os.system('cd %s && uv pip install -e . --config-settings editable_mode=compat' % path)
    # os.system('cd %s && %s setup.py develop' % (path, PYTHON))


def setup_install(path):
    title(path)
    os.system('cd %s && pip install -e .' % path)
    os.system('cd %s && %s setup.py install' % (path, PYTHON))


def search_setup():
    l = glob.glob('./**/pyproject.toml', recursive=True)
    result = []
    for k in l:
        path = '/'.join(k.split('/')[0:-1])
        if path in BASE:
            continue
        last = path.split('/')[-1]
        if last in BLACKLIST:
            continue
        result.append(path)

    return result


def run():
    required()
    for p in BASE:
        setup_develop(p)
    for p in search_setup():
        setup_develop(p)


if __name__ == '__main__':
    run()
