====================
xAAL Python packages
====================


This page contains a curated list of Python packages for xAAL.

* Most packages are available on `PyPI <https://pypi.org/search/?q=xaal>`_ and can be installed with pip: ``pip install xaal.<package>``
* Some packages are not yet on PyPI, you can install them from the Gitlab repository.

Libs
====
.. sectnum::

Commons libs used to develop applications and gateways.

.. table::
    :widths: 20, 80

    ==========================================================================================================================================  ============================================================
    Package                                                                                                                                     Description
    ==========================================================================================================================================  ============================================================
    `xaal.lib <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/libs/lib>`_                                                        core xAAL lib
    `xaal.monitor <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/libs/monitor>`_                                                lib used to monitor the all devices
    `xaal.schemas <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/libs/schemas>`_                                                devices skeletons generated from schemas
    ==========================================================================================================================================  ============================================================

Apps
====
General purpose applications: command line, dashboard and geeks stuff.

.. table::
    :widths: 20, 80

    ==========================================================================================================================================  ============================================================
    Package                                                                                                                                     Description
    ==========================================================================================================================================  ============================================================
    `xaal.tools <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/apps/tools>`_                                                    xAAL command line tools
    `xaal.legacytools <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/apps/legacytools>`_                                        same as xaal.tools but using the old lib (easy to read)
    `xaal.dashboard <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/apps/dashboard>`_                                            simple SocketIO xAAL dashboard
    `xaal.rest <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/apps/rest>`_                                                      REST webserver
    `xaal.fuse <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/apps/fuse>`_                                                      xAAL FUSE filesystem
    `xaal.conky <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/apps/conky>`_                                                    simple conky integration script
    ==========================================================================================================================================  ============================================================

Core
====
xAAL bus core devices.

.. table::
    :widths: 20, 80

    ==========================================================================================================================================  ============================================================
    Package                                                                                                                                     Description
    ==========================================================================================================================================  ============================================================
    `xaal.metadb <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/core/metadb>`_                                                  xAAL Metadata server
    ==========================================================================================================================================  ============================================================

Tests
=====
Fakes devices used to test the xAAL bus and automations.

.. table::
    :widths: 20, 80

    ==========================================================================================================================================  ============================================================
    Package                                                                                                                                     Description
    ==========================================================================================================================================  ============================================================
    `xaal.dummy <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/tests/dummy>`_                                           command line fake lamps, bots
    `xaal.fakeinput <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/tests/fakeinput>`_                                   web based fake contacts/switches/buttons
    ==========================================================================================================================================  ============================================================


Notifications
=============
xAAL devices than can receive notifications.

.. table::
    :widths: 20, 80

    ===================================================================================================================================================  ============================================================
    Package                                                                                                                                              Description
    ===================================================================================================================================================  ============================================================
    `xaal.gtknotify <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/notifications/gtk-notify>`_                                   Gtk notification script
    `xaal.pushbullet <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/notifications/pushbullet>`_                                  Phone (pushbullet) notification
    ===================================================================================================================================================  ============================================================


Protocols
=========
xAAL gateways for common protcols.

.. table::
    :widths: 20, 80

    =============================================================================================================================================  ============================================================
    Package                                                                                                                                        Description
    =============================================================================================================================================  ============================================================
    `xaal.zwave <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/ZWave>`_                                          ZWave gateway
    `xaal.knx <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/KNX>`_                                              KNX gateway
    `xaal.homekit <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/HomeKit>`_                                      HomeKit gateway
    `xaal.esphome <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/ESPHome>`_                                      ESPHome gateway
    `xaal.aqara <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/Aqara>`_                                          Aqara (Xiaomi) gateway
    `xaal.tuya <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/Tuya>`_                                            Tuya gateway
    `xaal.yeelight <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/Yeelight>`_                                    Yeelight gateway
    `xaal.meross <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/Meross>`_                                        Meross gateway
    `xaal.sensfloor <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/SensFloor>`_                                  SensFloor gateway
    `xaal.edisio <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/Edisio>`_                                        Edisio gateway
    `xaal.netatmo <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/Netatmo>`_                                      Netatmo gateway
    `xaal.ipx800 <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/IPX-800>`_                                       IPX-800 gateway
    `xaal.bugone <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/bugOne>`_                                        bugOne gateway
    `xaal.hq433 <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/protocols/HQ433>`_                                          Arduino serial gateway (example)
    =============================================================================================================================================  ============================================================


Other devices
=============
Some examples and test devices

.. table::
    :widths: 20, 80

    ============================================================================================================================================  ============================================================
    Package                                                                                                                                       Description
    ============================================================================================================================================  ============================================================
    `xaal.owm <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/weather/OpenWeatherMap>`_                                    OpenWeatherMap device (temperature / humidity / wind)
    `xaal.lmsensors <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/sensors/lm_sensors>`_                                  LM sensors gateway
    `xaal.htu21d <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/sensors/HTU21D>`_                                         HTU21D (temperature / humidity sensor) I2C example
    ============================================================================================================================================  ============================================================


Loggers
=======
xAAL devices used to log data.

.. table::
    :widths: 20, 80

    ============================================================================================================================================  ============================================================
    Package                                                                                                                                       Description
    ============================================================================================================================================  ============================================================
    `xaal.warp10 <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/loggers/warp10>`_                                         Push data into a warp10 DB.
    `xaal.mqtt <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/loggers/mqtt>`_                                             Push data into a mqtt server
    `xaal.influxdb <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/loggers/influxdb>`_                                     Push data into a influxdb server (unmaintained)
    ============================================================================================================================================  ============================================================


Emulations
==========
xAAL devices emulated in another protocol.

.. table::
    :widths: 20, 80

    ==========================================================================================================================================  ============================================================
    Package                                                                                                                                     Description
    ==========================================================================================================================================  ============================================================
    `xaal.fauxmo <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/emulations/Fauxmo>`_                                    Belkin WeMo emulation layer for xAAL (Alexa compliant)
    ==========================================================================================================================================  ============================================================


Assistants
==========
Support for voice assitants.

.. table::
    :widths: 20, 80

    ==========================================================================================================================================  ============================================================
    Package                                                                                                                                     Description
    ==========================================================================================================================================  ============================================================
    `xaal.alexa <https://gitlab.imt-atlantique.fr/xaal/code/python/-/tree/main/devices/assistants/Alexa>`_                                      Amazon Alexa support (Alexa TTS only right now)
    ==========================================================================================================================================  ============================================================


==

Last updated : 2024-09-25
