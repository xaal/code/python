#!/usr/bin/bash

python3 -m venv ~/xaal_env
source ~/xaal_env/bin/activate
# uv is faster than pip
pip install pip --upgrade pip
pip install uv
uv pip install xaal.tools xaal.dummy xaal.dashboard xaal.fakeinput xaal.owm

mkdir ~/.xaal/
echo xAAL|xaal-keygen|grep "key=" > ~/.xaal/xaal.ini

echo "source ~/xaal_env/bin/activate.fish" >> ~/.config/fish/config.fish
