This folder contains some docker images.

xaal-debian-dev:
================
This docker image is a an debian distro w/ the xAAL Git. The default xAAL key is "xAAL".
You can of course launch severals images in parallel to test the protocol itself.

xaal-debian-test:
=================
This docker image is an debian distro w/ the xAAL from PyPi. The default xAAL key is "xAAL".
This version only contains the xAAL python library, the tools and some fake or simple devices :

- xaal.dummy
- xaal.fakeinput
- xaal.dashboard
- xaal.owm
