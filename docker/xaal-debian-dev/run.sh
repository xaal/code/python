#!/bin/sh

VER=latest
NAME=xaal-debian-dev

docker buildx build -t ${NAME}:${VER} .
docker run -it --rm -p 9090:9090 -p 8080:8080 -p 8081:8081 ${NAME}:${VER}
