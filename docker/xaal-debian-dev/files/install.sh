#!/usr/bin/bash

pip install --upgrade pip
git clone https://gitlab.imt-atlantique.fr/xaal/code/python/ ./xaal_git
python3 -m venv ~/xaal_env
source ~/xaal_env/bin/activate

cd ~/xaal_git && python3 ./install.py

mkdir ~/.xaal/
echo xAAL|xaal-keygen|grep "key=" > ~/.xaal/xaal.ini

echo "source ~/xaal_env/bin/activate.fish" >> ~/.config/fish/config.fish
